FROM debian:stretch-slim

RUN apt-get update \
    && apt-get install -y git make default-libmysqlclient-dev zlib1g-dev libpcre3-dev gcc g++ procps \
    && apt-get autoremove -y \
    && apt-get clean
