# rathena-base
Base docker image to build rAthena server. It includes all the dependencies needed to compile rAthena.

# How to use
You can use this Dockerfile as a base for your own rAthena-based server's Dockerfile.

## Build rathena-base image
```shell
sudo docker build -t rathena-base .
```

## Use the built image to create your server's Docker image
Create a Dockerfile in the root of your server directory with the following content:
```shell
FROM rathena-base

COPY ./ /app/rathena-server/
WORKDIR /app/rathena-server

RUN chmod a+x configure && \
    ./configure --enable-packetver=[YOUR_CLIENT_PACKET_VER] --enable-64bit --enable-epoll \
    && make clean \
    && make server
RUN chmod a+x login-server && chmod a+x char-server && chmod a+x map-server

CMD [ "./athena-start", "watch" ]
```

Replace `[YOUR_CLIENT_PACKET_VER]` with your client's packet version and build the image like before:
```shell
sudo docker build -t my-cool-server .
```

## Run your server
To run your server you just need to run something like the following:
```shell
sudo docker run -it -v 6900:6900 -v 5121:5121 -v 6121:6121 --name=my-running-server my-cool-server
```

Of course you'll need a database to connect to. You can use the `mysql` image from Docker Hub.
```shell
sudo docker run -it -e MYSQL_ROOT_PASSWORD=[YOUR_ROOT_PASSWORD] -e MYSQL_DATABASE=[YOUR_DB_NAME] --name=my-running-db mysql:5.7.23
```

Replace the `[YOUR_ROOT_PASSWORD]` with a password for mysql's _root_ user, and `[YOUR_DB_NAME]` with the name of the database to create.

Lastly you need to create a common network between your server and your database, for which you could do the following:
```shell
sudo docker network create my-cool-server-network
sudo docker network connect my-cool-server-network my-running-server
sudo docker network connect my-cool-server-network my-running-db
```

Now your server should be able to connect to the MySQL database. Just adjust your database config in `conf/import-tmpl/inter_conf.txt` or `conf/import/inter_conf.txt`. Note with the above database config you will be using MySQL's `root` user.
